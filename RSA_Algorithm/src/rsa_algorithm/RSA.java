/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rsa_algorithm;

import java.math.BigInteger;
import java.security.SecureRandom;
/**
 *
 * @author David y Carlos
 */
public class RSA {
    
    private BigInteger n;
    private BigInteger e;
    private BigInteger d;
    
    private int nBits = 512;
    
    public RSA(int bits) {
        nBits = bits;
        SecureRandom random = new SecureRandom();
        // This class generates a Big Integer, in this case we are giving
        // the certainty of primality and a secure random number.
        BigInteger p = new BigInteger(nBits/2, 100, random);
        BigInteger q = new BigInteger(nBits/2, 100, random);
        
        // "n" contains the multiplying of "p" and "q".
        n = p.multiply(q);
        
        // Now we obtain the multiplying of (p-1) and (q-1), "mpq".
        BigInteger mpq = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        
        // We find "e" relativily prime with "mpq".
        e = new BigInteger(nBits/2 , 100, random);
        while (!mpq.gcd(e).equals(BigInteger.ONE)) {
            e = new BigInteger(nBits/2, 100, random);
        }
        // Finally we obtain the private exponent "d".
        d = e.modInverse(mpq);
    }
    
    public synchronized String encrypt(String m) {
        // We pow our message bytes to "e" modulus "n", then we return the result.
        return (new BigInteger(m.getBytes())).modPow(e, n).toString();
    }
    
    public synchronized String decrypt(String c) {
        // We pow our encrypted message to "d" modulus "n", then we return the result.
        return new String((new BigInteger(c)).modPow(d, n).toByteArray());
    }
}
